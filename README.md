Role Name
--------------------
envoy

Role Philosophy
---------------
Simple docker wrapper for envoy


Dependencies
------------
- Installed docker on host

OS
------------
- Tested on **Centos 7**